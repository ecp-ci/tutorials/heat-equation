#!/usr/bin/env bash
   
mkdir -p ${HOME}/spack-share/{opt,cache,mcache,modulefiles}
mkdir -p ${HOME}/spack
pushd ${HOME}/spack
    git init
    git remote set-url origin ${SPACK_REPO} || git remote add origin ${SPACK_REPO}
    git fetch origin --depth=1 --prune +refs/heads/*:refs/remotes/origin/* +refs/tags/*:refs/tags/*
    git checkout -f -q master
    git rev-parse HEAD
popd
