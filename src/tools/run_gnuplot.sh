#!/bin/bash

tmpfil=$1/$1_soln_final.curve
lenx=$(tail -1 $1/$1_soln_final.curve | tr -s ' ' | cut -d' ' -f2)

# Compute midpoint of wall
lenx2=$(
dc << EOF
4
k
$lenx
2.0
/
p
EOF
)

# Compute left end of pipe
p0=$(
dc << EOF
4
k
$lenx2
0.05
-
p
EOF
)

# Compute right end of pipe
p1=$(
dc << EOF
4
k
$lenx2
0.05
+
p
EOF
)


gnuplot << EOF 1>/dev/null 2>&1 &
set xlabel "Distance (meters)"
set ylabel "Temperature (Kelvin)"
set arrow 27 from 0,273 to $lenx,273 nohead lc rgb "blue"
set arrow 28 from $p0,273 to $p1,273 nohead lc rgb "black" lw 4
set term png
set output "wall_plot.png"
plot "$tmpfil" with lines
unset output
unset terminal
EOF
sleep 10
echo "Generated wall_plot.png"
exit 0
