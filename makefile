PTOOL ?= visit
RUNAME ?= heat_results
RM = rm -f

SRC = src/heat.C src/utils.C src/args.C src/exact.C src/ftcs.C src/upwind15.C src/crankn.C
OBJ = $(SRC:.C=.o)
EXE = heat

# Implicit rule for object files
%.o : %.C
	$(CXX) -c $(CXXFLAGS) $(CPPFLAGS) $< -o $@

# Linking the final heat app
heat: $(OBJ)
	$(CXX) -o heat $(OBJ) -lm

clean:
	$(RM) $(OBJ) $(EXE)

plot:
	@./src/tools/run_$(PTOOL).sh $(RUNAME)
